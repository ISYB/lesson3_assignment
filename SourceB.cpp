#include <sstream>
#include <iostream>
#include <string>
#include "sqlite3.h"
#include <vector>
#include <unordered_map>

using namespace std;

int price;
int balance;
int secondBalance;

unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int GetPrice(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
			price = stoi(p.second[0]);
		}
	}

	return 0;
}


int GetBalance(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
			balance = stoi(p.second[0]);
		}
	}
	return 0;
}

int GetSecondBalance(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
			secondBalance = stoi(p.second[0]);
		}
	}
	return 0;
}


int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}


void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}



bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	bool ans = false;
	int rc = 0;
	int moneyLeft = 0;
	string check = "";
	string tmp("SELECT balance from accounts where id = " + to_string(buyerid) + " ;");
	string tmp1("SELECT price from cars where id = "+to_string(carid)+" ;");	
	rc = sqlite3_exec(db, tmp.c_str(), GetBalance, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return ans;
	}
	clearTable();
	rc = sqlite3_exec(db, tmp1.c_str(), GetPrice, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return ans;
	}
	if (balance < price)
	{
		return ans;
	}
	else
	{
		moneyLeft = balance - price;
		
		tmp = "UPDATE accounts set balance = "+to_string(moneyLeft)+" where id = "+to_string(buyerid)+" ;";
		clearTable();
		rc = sqlite3_exec(db, tmp.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return ans;
		}
		
		tmp1 = "UPDATE cars set available = 0 where id = "+to_string(carid)+" ;";
		clearTable();
		rc = sqlite3_exec(db, tmp1.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return ans;
		}
	}
	ans = true;
	return ans;
}


bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc = 0;
	bool ans = false;
	int diff = 0;
	string tmp = "SELECT balance from accounts where id = " +to_string(from) +";";
	string tmp1 = "SELECT balance from accounts where id = " + to_string(to) + ";";

	//GETING FIRST BALANCE
	rc = sqlite3_exec(db, tmp.c_str(), GetBalance, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return ans;
	}

	clearTable();

	//GETTING SECOND BALANCE
	rc = sqlite3_exec(db, tmp1.c_str(), GetSecondBalance, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return ans;
	}

	clearTable();

	diff = balance - amount;
	if (diff < 0)
	{
		return ans;
	}
	else
	{
		//The balance that got the money.
		tmp1 = "UPDATE accounts set balance = "+to_string(secondBalance+amount)+" where id = " + to_string(to) + " ;";
		//The balance that gave the money.
		tmp = "UPDATE accounts set balance = " + to_string(balance - amount) + " where id = " + to_string(from) + " ;";
		
		//Update second balance.
		rc = sqlite3_exec(db, tmp1.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return ans;
		}

		clearTable();
		//Update first balance.
		rc = sqlite3_exec(db, tmp.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return ans;
		}
	}
	ans = true;
	return ans;
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	bool ans = balanceTransfer(1, 2, 300000, db, zErrMsg);//Transfer

	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	printTable();

	clearTable();
	rc = sqlite3_exec(db, "select * from cars", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	printTable();
	clearTable();

	ans = carPurchase(4, 23, db, zErrMsg);//success
	clearTable();
	rc = sqlite3_exec(db, "select * from cars", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	printTable();

	ans = carPurchase(1,13,db,zErrMsg);//success
	clearTable();
	rc = sqlite3_exec(db, "select * from cars", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	printTable();

	ans = carPurchase(1, 13, db, zErrMsg);//failure
	clearTable();
	rc = sqlite3_exec(db, "select * from cars", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	printTable();




	system("pause");
	return 0;
}